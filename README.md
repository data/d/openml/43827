# OpenML dataset: City-Quality-of-Life-Dataset

https://www.openml.org/d/43827

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Teleport.org provides data related to quality of life for the most creative cities around the world, which enables users to find suitable cities for living and working according to their personal preferences.
This dataset was obtained from Teleport API endpoints (https://developers.teleport.org/api/) and pre-processed before being uploaded to Kaggle.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43827) of an [OpenML dataset](https://www.openml.org/d/43827). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43827/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43827/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43827/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

